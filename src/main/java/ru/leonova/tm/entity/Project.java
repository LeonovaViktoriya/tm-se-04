package ru.leonova.tm.entity;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.UUID;

public class Project {

    private String name;
    private String prId;
    private String description;
    private Date dateStart;
    private Date dateEnd;

    {
        UUID.randomUUID();
    }

    public Project(String name) {
        this.name = name;
        prId = UUID.randomUUID().toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrID() {
        return prId;
    }

    public void setPrID(String prID) {
        this.prId = prID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

//    public void addTask(String taskName) {
//        Task newTask = new Task(taskName);
//        taskList.add(newTask);
//    }

}
