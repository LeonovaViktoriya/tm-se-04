package ru.leonova.tm.service;

import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.TaskRepository;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void createTask(Task t) {
        if(t==null)return;
        if(taskRepository.findAll().isEmpty()){
            taskRepository.persist(t);
        }else {
        taskRepository.merge(t);
        }
    }

    public void showTaskList() {
        LinkedHashMap<String, Task> taskMap = taskRepository.findAll();
        AtomicInteger i = new AtomicInteger();
        taskMap.forEach((k,v)-> System.out.println(i.getAndIncrement() + ". ID PROJECT: "+ taskMap.get(k).getPrID()+
                ". ID TASK: "+ taskMap.get(k).getTaskID()+ ", NAME: " + taskMap.get(k).getName()));
    }

    public void updateTaskName(Task t, String taskName) {
        if(t==null || taskName.isEmpty()) {
            System.out.println("This task or name is null");
            return;
        }
        t.setName(taskName);
    }

    public void deleteTasksByIdProject(String prID) {
        if(prID.isEmpty())return;
        LinkedHashMap<String, Task> taskMap = taskRepository.findAll();
        Iterator it = taskMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry taskIt = (Map.Entry)it.next();
            Task t = (Task) taskIt.getValue();
            if(t.getPrID().equals(prID)) {
                it.remove();
            }
        }
    }

    public boolean isEmptyTaskList() {
        return taskRepository.findAll().isEmpty();
    }

    public void deleteTask(Task t) {
        if(t==null)return;
        taskRepository.remove(t);
    }

    public Task findTaskById(String id)throws Exception{
        if(id.isEmpty()) throw new Exception("Id is null");
        return taskRepository.findOne(id);
    }

    public void deleteAllTask() {
        taskRepository.removeAll();
    }

}
