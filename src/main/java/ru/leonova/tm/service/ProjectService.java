package ru.leonova.tm.service;

import ru.leonova.tm.entity.Project;
import ru.leonova.tm.repository.ProjectRepository;

import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void showProjectList() {
        LinkedHashMap<String, Project> projectMap = projectRepository.findAll();
        AtomicInteger i=new AtomicInteger();
        projectMap.forEach((k, v)-> System.out.println(i.getAndIncrement() + ". ID PROJECT: "
                + projectMap.get(k).getPrID()+ ", NAME: " + projectMap.get(k).getName()));
    }

    public void createProject(Project project) {
        if(project==null)return;
        if(projectRepository.findAll().isEmpty()){
            projectRepository.persist(project);
        }else {
            projectRepository.merge(project);
        }
    }

    public void updateProject(Project p, String newName) {
        if(p == null || newName.isEmpty())return;
        p.setName(newName);
    }

    public boolean isEmptyProjectList() {
        return projectRepository.findAll().isEmpty();
    }

    public Project findProjectById(String id) throws Exception {
        if(id.isEmpty()) throw  new Exception("Null id");
        return projectRepository.findOne(id);
    }

    public void deleteProject(Project p) {
        if(p == null)return;
        projectRepository.remove(p);
    }

    public void deleteAllProject() {
        projectRepository.removeAll();
    }
}
