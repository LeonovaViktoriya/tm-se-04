package ru.leonova.tm.bootstrap;

import ru.leonova.tm.command.TerminalCommand;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.service.ProjectService;
import ru.leonova.tm.service.TaskService;

import java.util.Scanner;

public class Bootstrap {

    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);
    private Scanner scanner;

    public static void init() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        Scanner scanner = new Scanner(System.in);
        Bootstrap bootstrap = new Bootstrap(scanner);
        String text;
        do {
            text = scanner.nextLine();
            bootstrap.isCorrectCommand(text);
            switch (text.toLowerCase()) {
                case "create-p":
                    bootstrap.createProject();
                    break;
                case "create-t":
                    bootstrap.createTask();
                    break;
                case "list-p":
                    bootstrap.showProjectList();
                    break;
                case "list-t":
                    bootstrap.showTaskList();
                    break;
                case "del-p":
                    bootstrap.delProject();
                    break;
                case "del-t":
                    bootstrap.delTask();
                    break;
                case "up-p":
                    bootstrap.updateProject();
                    break;
                case "up-t":
                    bootstrap.updateTask();
                    break;
                case "del-all-p":
                    bootstrap.delAllProjects();
                    break;
                case "del-all-t":
                    bootstrap.delAllTasks();
                    break;
                case "del-all-t-t-p":
                    bootstrap.delAllTasksOfProject();
                    break;
                case "help":
                    bootstrap.showAllCommands();
                    break;
            }
        }
        while (!text.equals("exit"));
    }

    private Bootstrap(Scanner scanner) {
        this.scanner = scanner;
    }

    private void createProject(){
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        String name = scanner.nextLine();
        Project project = new Project(name);
        projectService.createProject(project);
        System.out.println("Project created");
    }

    private void isCorrectCommand(String s) {
        TerminalCommand[] menuCommands = TerminalCommand.values();
        boolean equals=false;
        for (TerminalCommand m : menuCommands) {
            if(m.getCommand().equals(s)){equals=true;}
        }
        if(!equals){
            System.out.println("Is not correct value. Try again\n");
        }
    }

    private void createTask(){
        if (projectService.isEmptyProjectList()) {
            System.out.println("You have no project for task");
        }else {
            System.out.println("[CREATE TASK]\nList projects:");
            projectService.showProjectList();
            System.out.println("\nSELECT ID PROJECT: ");
            String id = scanner.nextLine();
            System.out.println("Enter name task: ");
            Scanner input = new Scanner(System.in);
            String name = input.nextLine();
            Task t = new Task(name, id);
            taskService.createTask(t);
            System.out.println("Task created");
        }
    }

    private void delProject() throws Exception {
        if (projectService.isEmptyProjectList()) {
            System.out.println("You have no projects");
        }else {
            System.out.println("[DELETE PROJECT BY ID]\nList projects:");
            projectService.showProjectList();
            System.out.println("Enter id project:");
            String id = scanner.nextLine();
            Project p = projectService.findProjectById(id);
            if (p == null) {
                System.out.println("This project not found");
            }else {
                taskService.deleteTasksByIdProject(id);
                projectService.deleteProject(p);
                System.out.println("Project with id: " + p.getPrID() + " with his tasks are removed");
            }
        }
    }

    private void updateProject() throws Exception {
        if (projectService.isEmptyProjectList()) {
            System.out.println("\nYou have no projects");
        }else {
            System.out.println("[UPDATE NAME PROJECT]\nList projects");
            projectService.showProjectList();
            System.out.println("Enter id project:");
            Project p = projectService.findProjectById(scanner.nextLine());
            if (p == null) {
                System.out.println("Project with this id not found");
            }else {
                System.out.println("Enter new name for this project:");
                String name = scanner.nextLine();
                projectService.updateProject(p, name);
                System.out.println("Task " + p.getName() + " modified like " + name);
            }
        }
    }

    private void updateTask() throws Exception {
        if (taskService.isEmptyTaskList()) {
            System.out.println("You have no tasks");
        }else {
            System.out.println("[UPDATE NAME TASK]\nList tasks:");
            taskService.showTaskList();
            System.out.println("Enter id task for rename:");
            Task t = taskService.findTaskById(scanner.nextLine());
            if (t == null) {
                System.out.println("This task not found");
            }else {
                System.out.println("enter new name for task:");
                String name = scanner.nextLine();
                taskService.updateTaskName(t, name);
                System.out.println("Task " + t.getName() + " modified like " + name);
            }
        }
    }

    private void delAllTasksOfProject(){
        System.out.println("[DELETE ALL TASKS OF THE SELECTED PROJECT]\n");
        projectService.showProjectList();
        System.out.println("Select ID project: ");
        taskService.deleteTasksByIdProject(scanner.nextLine());
        System.out.println("All tasks of this project are deleted");
    }

    private void showProjectList(){
        if (projectService.isEmptyProjectList()) {
            System.out.println("You have no projects");
        }else {
            System.out.println("[PROJECT LIST]");
            projectService.showProjectList();
        }
    }

    private void showTaskList(){
        if (taskService.isEmptyTaskList()) {
            System.out.println("You have no tasks");
        }else {
            System.out.println("[TASK LIST]");
            taskService.showTaskList();
        }
    }

    private void delTask() throws Exception {
        if (taskService.isEmptyTaskList()) {
            System.out.println("You have no tasks");
        }else {
            System.out.println("[DELETE TASK BY ID]\n[Task list:]");
            taskService.showTaskList();
            System.out.println("Enter task id:");
            String idTask = scanner.nextLine();
            Task t = taskService.findTaskById(idTask);
            if (t == null) {
                System.out.println("This task not found");
            }else {
                taskService.deleteTask(t);
                System.out.println("Task " + t.getName() + " remove");
            }
        }
    }

    private void delAllProjects(){
        taskService.deleteAllTask();
        projectService.deleteAllProject();
        System.out.println("All projects remove");
    }

    private void delAllTasks(){
        taskRepository.removeAll();
        System.out.println("All tasks remove");
    }

    private void showAllCommands(){
        System.out.println("help - show available commands");
        System.out.println("create-p - create new project");
        System.out.println("create-t - create new task for current project");
        System.out.println("list-p - show project list");
        System.out.println("list-t - show project list");
        System.out.println("del-p - delete project by id with all his tasks");
        System.out.println("del-t - delete task by id");
        System.out.println("up-p - update project");
        System.out.println("up-t - update project's task's name");
        System.out.println("del-all-t - delete all task");
        System.out.println("del-all-p - delete all projects");
        System.out.println("del-all-t-t-p - delete all task of the selected project");
    }

}

