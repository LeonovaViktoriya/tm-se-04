package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Project;

import java.util.LinkedHashMap;

public class ProjectRepository {

    private LinkedHashMap<String, Project> projectMap = new LinkedHashMap();

    public void persist(Project p) {
        projectMap.put(p.getPrID(), p);
    }

    public LinkedHashMap<String, Project> findAll() {
        return projectMap;
    }

    public Project findOne(String id) {
        for (String idP : projectMap.keySet()) {
            if (idP.equals(id)) {
                return projectMap.get(id);
            }
        }
        return null;
    }

    private void updateProjectName(Project p, String newName) {
        p.setName(newName);
    }

    public Project merge(Project p) {
        for (Project project : projectMap.values()) {
            if (project.getPrID().equals(p.getPrID())) {
                updateProjectName(project, p.getName());
            } else {
                persist(p);
            }
        }
        return p;
    }

    public void remove(Project p) {
        projectMap.remove(p.getPrID());
    }

    public void removeAll() {
        projectMap.clear();
    }

}
