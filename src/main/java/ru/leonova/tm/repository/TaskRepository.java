package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Task;

import java.util.LinkedHashMap;

public class TaskRepository {
    //id task, task
    private LinkedHashMap<String, Task> taskMap = new LinkedHashMap();

    public void persist(Task task) {
        taskMap.put(task.getTaskID(), task);
    }

    private void updateTaskName(Task t, String taskName) {
        t.setName(taskName);
    }

    public void merge(Task task){
        for (Task t : taskMap.values()) {
            if(t.getTaskID().equals(task.getTaskID())){
                updateTaskName(t, task.getName());
            }else {
                persist(task);
            }
        }
    }

    public LinkedHashMap<String, Task> findAll(){
        return taskMap;
    }

    public Task findOne(String idTask) {
        for (Task task: taskMap.values()) {
            if (task.getTaskID().equals(idTask)) {
                return task;
            }
        }
        return null;
    }

    public void remove(Task t) {
        taskMap.remove(t.getTaskID());
    }

    public void removeAll() {
        taskMap.clear();
    }

}

